var Crawler = require('crawler').Crawler;
var fs = require("fs");

write = fs.createWriteStream('crawled.txt');

write.on('error', function(err){
  console.log(err);
});

var getPost = function(error, result, $) {
		 var post = {}
     post.title = $('.entry-title').text();
     post.content = $('.entry-content').html();
     post.date = $('.post-date').text();
     console.log("====" + post.title + "====");
     write.write(JSON.stringify(post));
};

var pages = function(error, result, $) {
   $next = $('.nav-previous a');
   $posts = $(".type-post .more-link");

   $next.each(function(index, a){
      c.queue(
        [{
          "uri": a.href,
          "callback": pages
        }]
      );
    });

   $posts.each(function(index, a ){
    c.queue(
      [{
        "uri": a.href,
        "callback": getPost
      }]
    );
   });
};

var c = new Crawler({
  "maxConnections": 10,
  "callback": pages
});

c.queue("http://addyosmani.com/blog/");

/*
for each top level page, grab all the posts on the page,
AND grab the next page link, if it exists.
*/
